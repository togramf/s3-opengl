#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "glimac/glm.hpp"
#include <vector>

using namespace glimac;
const float pi = 3.1415;

struct Vertex2DColor {
    //attributs
    glm::vec2 position;
    glm::vec3 color;

    //constructeurs
    Vertex2DColor(const glm::vec2 p, const glm::vec3 c);
    Vertex2DColor();
    
};

Vertex2DColor::Vertex2DColor(const glm::vec2 p, const glm::vec3 c){
    position.x = p.x; position.y = p.y;
    color.x = c.x; color.y = c.y; color.z = c.z;
}

Vertex2DColor::Vertex2DColor(){
    position = glm::vec2(0.0,0.0);
    color = glm::vec3(0.0,0.0,0.0);
}

int main(int argc, char** argv) {
    // Initialize SDL and open a window
    SDLWindowManager windowManager(800, 600, "GLImac");

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    FilePath applicationPath(argv[0]);
    Program program = loadProgram(applicationPath.dirPath() + "shaders/triangle.vs.glsl",
                                applicationPath.dirPath() + "shaders/triangle.fs.glsl");
    
    program.use();

    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/
    //Création d'un seul VBO
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    // => Tableau de sommets : un seul exemplaire de chaque sommet
   const int NB_TRIANGLES_CERCLE = 20; 
    const float RAYON = 0.5;

    std::vector<Vertex2DColor> vertices;
    vertices.push_back(Vertex2DColor(glm::vec2(0.0,0.0), glm::vec3(1,0,0)));

    for (int i=0; i<NB_TRIANGLES_CERCLE; i++){
        vertices.push_back(Vertex2DColor(glm::vec2(RAYON * cos(i*2*pi/NB_TRIANGLES_CERCLE), RAYON * sin(i*2*pi/NB_TRIANGLES_CERCLE)), glm::vec3(0,0,1)));
    }

    // => Penser à bien changer le nombre de sommet (4 au lieu de 6):
    glBufferData(GL_ARRAY_BUFFER, (NB_TRIANGLES_CERCLE+1) * sizeof(Vertex2DColor), vertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // => Creation du IBO
    GLuint ibo;
    glGenBuffers(1, &ibo);

    // => On bind sur GL_ELEMENT_ARRAY_BUFFER, cible reservée pour les IBOs
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    // => Tableau d'indices: ce sont les indices des sommets à dessiner
    uint32_t indices[3*(NB_TRIANGLES_CERCLE+1)];
    for (int i=0; i<NB_TRIANGLES_CERCLE-1; i++){
        indices[i*3]=0;
        indices[i*3+1]=i+1;
        indices[i*3+2]=i+2;
    }
    indices[3*NB_TRIANGLES_CERCLE]=0;
    indices[3*NB_TRIANGLES_CERCLE+1]=NB_TRIANGLES_CERCLE;
    indices[3*NB_TRIANGLES_CERCLE+2]=1;

    // => On remplit l'IBO avec les indices:
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, (NB_TRIANGLES_CERCLE * 3 + 3)* sizeof(uint32_t), indices, GL_STATIC_DRAW);

    // => Comme d'habitude on debind avant de passer à autre chose
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    
    //Création de la VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // => On bind l'IBO sur GL_ELEMENT_ARRAY_BUFFER; puisqu'un VAO est actuellement bindé,
    // cela a pour effet d'enregistrer l'IBO dans le VAO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    //Spécification des attributs de Vertex
    const GLuint VERTEX_ATTR_POSITION = 3;
    const GLuint VERTEX_ATTR_COLOR = 8;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_COLOR);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const void*)(offsetof(Vertex2DColor,position)));
    glVertexAttribPointer(VERTEX_ATTR_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const void*)(offsetof(Vertex2DColor,color)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Application loop:
    bool done = false;
    while(!done) {
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true; // Leave the loop after this iteration
            }
        }

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
        glClear(GL_COLOR_BUFFER_BIT);

        glBindVertexArray(vao);
        // => On utilise glDrawElements à la place de glDrawArrays
        // Cela indique à OpenGL qu'il doit utiliser l'IBO enregistré dans le VAO
        glDrawElements(GL_TRIANGLES, NB_TRIANGLES_CERCLE *3 + 3, GL_UNSIGNED_INT, 0);

        glBindVertexArray(0);

        // Update the display
        windowManager.swapBuffers();
    }

    glDeleteBuffers(1,&vbo);
    glDeleteVertexArrays(1, &vao);
    return EXIT_SUCCESS;
}
