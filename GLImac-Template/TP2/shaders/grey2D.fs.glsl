#version 300 es

precision mediump float; 

in vec3 vFragColor;

out vec3 fFragColor; 

void main() {           
     
    fFragColor.x = (vFragColor.x + vFragColor.y + vFragColor.z)/3.0;
    fFragColor.y = (vFragColor.x + vFragColor.y + vFragColor.z)/3.0;
    fFragColor.z = (vFragColor.x + vFragColor.y + vFragColor.z)/3.0;
};
