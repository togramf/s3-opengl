#version 300 es

layout(location = 0) in vec2 aVertexPosition;
layout(location = 1) in vec2 aVertexTexture;

out vec2 vFragTexture;

uniform mat3 uModelMatrix;

void main() {
    vFragTexture = aVertexTexture;
    gl_Position = vec4((uModelMatrix*vec3(aVertexPosition,1.)).xy, 0, 1);

};
