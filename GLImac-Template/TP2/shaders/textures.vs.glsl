#version 300 es

layout(location = 0) in vec2 aVertexPosition;
layout(location = 1) in vec3 aVertexColor;

out vec3 vFragColor;
out vec3 vFragPosition;

void main() {

    vFragColor = aVertexColor;
    vFragPosition = vec3(aVertexPosition, 1.f);

    //Transformations de la position//
    //mat4 Mtranslation = mat4(vec4(0.1f, 0.f, 0.f, 0.f), vec4(0.f, 0.1f, 0.f, 0.f), vec4(0.f, 0.f, 1.f,0.f), vec4(5.0f, 0.f, 0.f, 1.f));
    //mat4 Mrescale = mat4(vec4(0.5f, 0.f, 0.f, 0.f), vec4(0.f, 0.5f, 0.f, 0.f), vec4(0.f, 0.f, 1.f, 0.f), vec4(0.f, 0.f, 0.f, 1.f));

    gl_Position = vec4(aVertexPosition.x , aVertexPosition.y, 0, 1);
};
