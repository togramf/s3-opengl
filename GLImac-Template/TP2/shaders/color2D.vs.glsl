#version 300 es

layout(location = 0) in vec2 aVertexPosition;
layout(location = 1) in vec3 aVertexColor;

out vec3 vFragColor;

void main() {
    vFragColor = aVertexColor;
    // mat3 M = mat3(vec3(1.f, 2.f, 3.f), vec3(4.f, 5.f, 6.f), vec3(7.f, 8.f, 9.f));
    // gl_Position = vec4((M * vec3(aVertexPosition, 1)).xy, 0, 1);
    gl_Position = vec4(aVertexPosition, 0, 1);
};
