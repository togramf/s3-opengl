#version 300 es

precision mediump float; 

in vec2 vFragTexture;

out vec3 fFragColor;

uniform vec3 uColor;
uniform sampler2D uTexture;

void main() {
    fFragColor = uColor;
    fFragColor = texture(uTexture, vFragTexture).xyz;
};
