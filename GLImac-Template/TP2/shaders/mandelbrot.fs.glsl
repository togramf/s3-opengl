#version 300 es
precision mediump float; 

in vec3 vColor;
in vec2 vPosition;

out vec3 fFragColor;

vec2 complexSqr(vec2 z){
    return vec2(z.x*z.x-z.y*z.y,2.*z.x*z.y); 
};

vec3 mandelbrot(vec2 z){
    float N_max = 500.0;
    vec2 zn = vec2(z.x,z.y);
    for (float n=0.0; n<N_max; n++){
        zn = complexSqr(zn)+z;
        if (length(zn) > 2.0)
            return vec3(n/N_max,n/N_max,n/N_max);
    }
    return vec3(0.,0.,0.);
};

void main() {
    fFragColor = mandelbrot(vPosition);
};
