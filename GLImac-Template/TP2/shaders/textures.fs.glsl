#version 300 es

precision mediump float; 

in vec3 vFragColor;
in vec3 vFragPosition; 

vec3 centre_triangle = vec3(0.0,0.0,1.0);

out vec3 fFragColor; 

void main() {           
    vec3 P = vFragPosition - centre_triangle;
    
    fFragColor = vFragColor * length(abs(fract(2.0 * P) * 4.0 - 0.5));
};
