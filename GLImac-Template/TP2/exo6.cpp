#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include "../glimac/include/glimac/Vertex2DUV.hpp"
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "glimac/glm.hpp"

using namespace glimac;

glm::mat3 translate (float x, float y){
    return glm::mat3(glm::vec3(1.f,0.f,0.f),glm::vec3(0.f,1.f,0.f),glm::vec3(x, y, 1.f)); 
}

glm::mat3 scale (float x, float y){
    return glm::mat3(glm::vec3(x, 0.f,0.f), glm::vec3(0.f, y, 0.f),glm::vec3(0.f, 0.f, 1.f));
}

glm::mat3 rotate (float a){
    return glm::mat3(glm::vec3(cos(a), sin(a), 0.f), glm::vec3(-sin(a), cos(a), 0.f), glm::vec3(0.f, 0.f, 1.f));
}

int main(int argc, char** argv) {
    // Initialize SDL and open a window
    SDLWindowManager windowManager(800, 600, "GLImac");

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    FilePath applicationPath(argv[0]);
    Program program = loadProgram(applicationPath.dirPath() + "shaders/tex2D_exo6.vs.glsl",
                                applicationPath.dirPath() + "shaders/tex2D_exo6.fs.glsl");
    
    program.use();

    //recup localisation variable uniforme 
    GLint locColor = glGetUniformLocation(program.getGLId(),"uColor");
    GLint locMatrix = glGetUniformLocation(program.getGLId(),"uModelMatrix");

    // std::cout << "location : " << loc << std::endl;
    //glUniform1f pour modifier uTime; ici 45° 
    // glUniform1f(0, 45.);

    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/

    //Création d'un seul VBO
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Création coordonnées des points du triangle
    Vertex2DUV vertices[] = {
        Vertex2DUV(-1.,-1.,0.,0.),
        Vertex2DUV(1.,-1.,0.,0.),  
        Vertex2DUV(0.,1.,0.,0.),     
    };
    glBufferData (GL_ARRAY_BUFFER, 3 * sizeof(Vertex2DUV), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Création de la VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    //Spécification des attributs de Vertex
    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_COLOR = 1;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_COLOR);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(double), (const void*)(offsetof(Vertex2DUV,_position)));
    glVertexAttribPointer(VERTEX_ATTR_COLOR, 2, GL_FLOAT, GL_FALSE, sizeof(double), (const void*)(offsetof(Vertex2DUV,_texture)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Application loop:
    float angle = 0.;
    glm::mat3 uMatrix1 = translate(-.5f,.5f)*scale(0.2,0.2);
    glm::mat3 uMatrix2 = translate(.5f,.5f)*scale(0.2,0.2);
    glm::mat3 uMatrix3 = translate(.5f,-.5f)*scale(0.2,0.2);
    glm::mat3 uMatrix4 = translate(-.5f,-.5f)*scale(0.2,0.2);
    glm::vec3 uVecColor1 = glm::vec3(0.,0.,0.);
    glm::vec3 uVecColor2 = glm::vec3(0.,0.,0.);
    glm::vec3 uVecColor3 = glm::vec3(0.,0.,0.);
    glm::vec3 uVecColor4 = glm::vec3(0.,0.,0.);

    bool done = false;
    while(!done) {
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true; // Leave the loop after this iteration
            }
        }

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
        glClear(GL_COLOR_BUFFER_BIT);

        glBindVertexArray(vao);
        //1er quart 
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(uMatrix1 * rotate(angle)));
        glUniform3fv(locColor, 1, glm::value_ptr(uVecColor1 + glm::vec3(angle,0.,0.)));
        glDrawArrays(GL_TRIANGLES, 0, 3);

        //2eme quart
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(uMatrix2*rotate(-angle)));
        glUniform3fv(locColor, 1, glm::value_ptr(uVecColor2 + glm::vec3(0.,angle,0.)));
        glDrawArrays(GL_TRIANGLES, 0, 3);

        //3e quart
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(uMatrix3*rotate(2*angle)));
        glUniform3fv(locColor, 1, glm::value_ptr(uVecColor3 + glm::vec3(0.,0.,angle)));
        glDrawArrays(GL_TRIANGLES, 0, 3);

        //4e quart 
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(uMatrix4*rotate(-angle*2)));
        glUniform3fv(locColor, 1, glm::value_ptr(uVecColor4 + glm::vec3(angle,0.,angle)));
        glDrawArrays(GL_TRIANGLES, 0, 3);


        glBindVertexArray(0);

        // Update the display
        windowManager.swapBuffers();
        angle += 0.01;
    }

    glDeleteBuffers(1,&vbo);
    glDeleteVertexArrays(1, &vao);
    return EXIT_SUCCESS;
}
