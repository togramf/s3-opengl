#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include "../glimac/include/glimac/Vertex2DUV.hpp"
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "glimac/glm.hpp"
#include "glimac/Image.hpp"

using namespace glimac;

glm::mat3 translate (float x, float y){
    return glm::mat3(glm::vec3(1.f,0.f,0.f),glm::vec3(0.f,1.f,0.f),glm::vec3(x, y, 1.f)); 
}

glm::mat3 scale (float x, float y){
    return glm::mat3(glm::vec3(x, 0.f,0.f), glm::vec3(0.f, y, 0.f),glm::vec3(0.f, 0.f, 1.f));
}

glm::mat3 rotate (float a){
    return glm::mat3(glm::vec3(cos(a), sin(a), 0.f), glm::vec3(-sin(a), cos(a), 0.f), glm::vec3(0.f, 0.f, 1.f));
}

int main(int argc, char** argv) {
    // Initialize SDL and open a window
    SDLWindowManager windowManager(800, 600, "GLImac");

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    //chargement des textures
    std::unique_ptr<Image> triforce = loadImage ("/home/6ima2/elvina.chevalier/Documents/OpenGL/GLImac-Template/assets/textures/triforce.png");
    if(!triforce) {
        std::cerr << "oups ptr texture pabon" << std::endl;
        return EXIT_FAILURE;
    }

    //chargement des shaders
    FilePath applicationPath(argv[0]);
    Program program = loadProgram(applicationPath.dirPath() + "shaders/tex2D.vs.glsl",
                                applicationPath.dirPath() + "shaders/tex2D.fs.glsl");
    
    program.use();

    //recup localisation variable uniforme 
    GLint locColor = glGetUniformLocation(program.getGLId(),"uColor");
    GLint locMatrix = glGetUniformLocation(program.getGLId(),"uModelMatrix");
    GLint locTexture = glGetUniformLocation(program.getGLId(),"uTexture");

    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/

    //Création d'un VBO
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Création coordonnées des points du triangle
    Vertex2DUV vertices[] = {
        Vertex2DUV(-.5,0., 0.,1.),
        Vertex2DUV(0.5,0., 1.,1.),  
        Vertex2DUV(0.,0.25, 0.5,0.),     
    };
    glBufferData (GL_ARRAY_BUFFER, 3 * sizeof(Vertex2DUV), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Création de la VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    //Spécification des attributs de Vertex
    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_COLOR = 1;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_COLOR);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(double), (const void*)(offsetof(Vertex2DUV,_position)));
    glVertexAttribPointer(VERTEX_ATTR_COLOR, 2, GL_FLOAT, GL_FALSE, sizeof(double), (const void*)(offsetof(Vertex2DUV,_texture)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Création de textures 
    GLuint textures;
    glGenTextures(1, &textures);
    glBindTexture(GL_TEXTURE_2D, textures);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, triforce->getWidth(), triforce->getHeight(), 0, GL_RGBA, GL_FLOAT, triforce->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Application loop:
    float angle = 0.;
    glm::mat3 uMatrix = scale(0.4,0.4);
    
    glm::vec3 uVecColor1 = glm::vec3(0.,0.,0.);
    glm::vec3 uVecColor2 = glm::vec3(0.,0.,0.);
    glm::vec3 uVecColor3 = glm::vec3(0.,0.,0.);
    glm::vec3 uVecColor4 = glm::vec3(0.,0.,0.);

    bool done = false;
    while(!done) {
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true; // Leave the loop after this iteration
            }
        }

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
        glClear(GL_COLOR_BUFFER_BIT);

        glBindVertexArray(vao);
        glBindTexture(GL_TEXTURE_2D, textures);
        //1er quart 
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(rotate(angle) * translate(0.5f,0.5f) * uMatrix ));
        glUniform1i(locTexture, 0);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // //2eme quart
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(rotate(angle) * translate(-0.5f,0.5f) * uMatrix ));
        glUniform1i(locTexture, 0);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // //3e quart
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(rotate(angle) * translate(0.5f,-0.5f) * uMatrix ));
        glUniform1i(locTexture, 0);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // //4e quart 
        glUniformMatrix3fv(locMatrix, 1, GL_FALSE, glm::value_ptr(rotate(angle) * translate(-0.5f,-0.5f) * uMatrix ));
        glUniform1i(locTexture, 0);
        glDrawArrays(GL_TRIANGLES, 0, 3);


        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // Update the display
        windowManager.swapBuffers();
        angle += 0.01;
    }

    glDeleteBuffers(1,&vbo);
    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1, &textures);
    return EXIT_SUCCESS;
}
