#version 300 es

precision mediump float; 

in vec3 vVertexPosition;
in vec3 vVertexNormal;
in vec2 vVertexCoords;

uniform vec3 uKd;               //Kd
uniform vec3 uKs;               //Ks
uniform float uShininess;
uniform vec3 uLightDir_vs;      // wi (à normaliser), dans le view space
uniform vec3 uLightIntensity;   // Li

out vec3 fFragColor; 

vec3 blinnPhong(){
    vec3 lightDir = normalize(uLightDir_vs);
    vec3 halfVector = 0.5*(normalize(-vVertexPosition)+lightDir);
    vec3 membre1 = uKd*dot(lightDir,vVertexNormal);
    vec3 membre2 = uKs*pow(dot(halfVector,vVertexNormal),uShininess);
    return cross(uLightIntensity,membre1+membre2);
}

void main() {
    fFragColor = blinnPhong();
    //fFragColor = vec3(1.,1.,1.);
};
