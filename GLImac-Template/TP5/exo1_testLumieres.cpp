#include <GL/glew.h>
#include <iostream>
#include <vector>

#include "glimac/Sphere.hpp"
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "glimac/glm.hpp"
#include <glimac/SDLWindowManager.hpp>
#include "glimac/Image.hpp"
#include "glimac/TrackballCamera.hpp"
#include "glimac/FreeflyCamera.hpp"

using namespace glimac;

struct mainProgram {
    Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uKd;
    GLint uKs;
    GLint uShininess;
    GLint uLightDir_vs;
    GLint uLightIntensity;

    mainProgram(const FilePath& applicationPath):
        m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                              applicationPath.dirPath() + "shaders/directionallights.fs.glsl")) {
        
        uMVPMatrix      = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix       = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix   = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
        
        uKd             = glGetUniformLocation(m_Program.getGLId(), "uKd");
        uKs             = glGetUniformLocation(m_Program.getGLId(), "uKs");
        uShininess      = glGetUniformLocation(m_Program.getGLId(), "uShininess");
        uLightDir_vs    = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
        uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
    }
};


int main(int argc, char** argv) {
    // Initialize SDL and open a window
    SDLWindowManager windowManager(800, 600, "GLImac");

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    //chargement des shaders
    FilePath applicationPath(argv[0]);
    mainProgram mainProgram(applicationPath);

    glEnable(GL_DEPTH_TEST); 
    
    //création des cameras
    TrackballCamera* camTrackball = new TrackballCamera();
    FreeflyCamera*   camFreefly   = new FreeflyCamera();
    
    //paramètres des cameras 
    float pasT = 5.0;
    float pasF_move = 0.1;
    float pasF_angle = 1.0;
    bool camTisActive = true;

    //initialisation des matrices 
    glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f),(float)800/(float)600,0.1f,100.f);
    glm::mat4 VMatrix = camTrackball->getViewMatrix();
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(VMatrix));

    
    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/
    
    //creation d'une sphère
    Sphere sphere(1,10,10);

    //Création d'un VBO
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData (GL_ARRAY_BUFFER, sphere.getVertexCount() * sizeof(ShapeVertex), sphere.getDataPointer(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Création de la VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_NORMALE = 1;
    const GLuint VERTEX_ATTR_COORDS = 2;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_COORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMALE);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex) , (const void*)(offsetof(ShapeVertex,position)));
    glVertexAttribPointer(VERTEX_ATTR_NORMALE, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void*)(offsetof(ShapeVertex,normal)));
    glVertexAttribPointer(VERTEX_ATTR_COORDS, 2, GL_FLOAT, GL_FALSE,sizeof(ShapeVertex), (const void*)(offsetof(ShapeVertex,texCoords)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Tirage des 32 axes de rotation des Lunes
    // std::vector<glm::vec3> axesRotLunes;
    // std::vector<glm::vec3> originLunes;
    // float radiusRotLunes = 2.0;
    // uint32_t nbmoon = 32;

    // for (uint32_t i=0; i<nbmoon; ++i){
        
    //     glm::vec3 axe1 = glm::sphericalRand(radiusRotLunes);
    //     glm::vec3 axe2 = glm::sphericalRand(radiusRotLunes);
    //     glm::vec3 axe3 = glm::cross(axe1,axe2);
    //     float radiusLunes = glm::linearRand(1.5,3.0);

    //     while (glm::distance(glm::vec3(0.f,0.f,0.f),axe3)<1.0){
    //         axe1 = glm::sphericalRand(radiusRotLunes);
    //         axe2 = glm::sphericalRand(radiusRotLunes);
    //         axe3 = glm::cross(axe1,axe2);
    //     }
    //     axesRotLunes.push_back(glm::normalize(axe1));
    //     originLunes.push_back(glm::normalize(axe3) * radiusLunes);
    // }

    // Application loop:
    bool done = false;
    while(!done) {
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true; // Leave the loop after this iteration
            }
        }
        if (windowManager.isKeyPressed(SDLK_c)){
            if (camTisActive)
                std::cout << "Freefly" <<std::endl;
            else 
                std::cout << "Trackball" <<std::endl;
            camTisActive = !camTisActive;
        }
        
        if(camTisActive){
            if (windowManager.isKeyPressed(SDLK_z)||windowManager.isKeyPressed(SDLK_UP)){
                camTrackball->rotateUp(pasT);
            } else if (windowManager.isKeyPressed(SDLK_s)||windowManager.isKeyPressed(SDLK_DOWN)){
                camTrackball->rotateUp(-pasT);
            } else if (windowManager.isKeyPressed(SDLK_q)||windowManager.isKeyPressed(SDLK_LEFT)){
                camTrackball->rotateLeft(pasT);
            } else if (windowManager.isKeyPressed(SDLK_d)||windowManager.isKeyPressed(SDLK_RIGHT)){
                camTrackball->rotateLeft(-pasT);
            } else if (windowManager.isKeyPressed(SDLK_LSHIFT)||windowManager.isKeyPressed(SDLK_RSHIFT)){
                camTrackball->moveFront(pasT/10);
            } else if (windowManager.isKeyPressed(SDLK_LCTRL)||windowManager.isKeyPressed(SDLK_RCTRL)){
                camTrackball->moveFront(-pasT/10);
            } else if (windowManager.isKeyPressed(SDLK_i)){
                camTrackball->initialise();
            } 
        } else {
            if (windowManager.isKeyPressed(SDLK_z)){
                camFreefly->moveFront(pasF_move);
            } else if (windowManager.isKeyPressed(SDLK_s)){
                camFreefly->moveFront(-pasF_move);
            } else if (windowManager.isKeyPressed(SDLK_q)){
                camFreefly->moveLeft(pasF_move);
            } else if (windowManager.isKeyPressed(SDLK_d)){
                camFreefly->moveLeft(-pasF_move);
            } else if (windowManager.isKeyPressed(SDLK_UP)){
                camFreefly->rotateUp(pasF_angle);
            } else if (windowManager.isKeyPressed(SDLK_DOWN)){
                camFreefly->rotateUp(-pasF_angle);
            } else if (windowManager.isKeyPressed(SDLK_RIGHT)){
                camFreefly->rotateLeft(-pasF_angle);
            } else if (windowManager.isKeyPressed(SDLK_LEFT)){
                camFreefly->rotateLeft(pasF_angle);
            } else if (windowManager.isKeyPressed(SDLK_i)){
                camFreefly->initialise();
            }
        }

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        if (camTisActive)
            VMatrix = camTrackball->getViewMatrix();
        else 
            VMatrix = camFreefly->getViewMatrix();

        glm::vec3 LightDir(1.,1.,1.);
        glm::vec3 LightIntensity(200,200,200);
        glm::vec3 Kd(50,50,50);
        glm::vec3 Ks(50,50,50);
        glBindVertexArray(vao);
        mainProgram.m_Program.use();
        
        // Planete
        glm::mat4 earthMVMatrix = glm::rotate(VMatrix, windowManager.getTime(), glm::vec3(0, 1, 0));
        
        glUniformMatrix4fv(mainProgram.uMVMatrix, 1, GL_FALSE, 
            glm::value_ptr(earthMVMatrix));
        glUniformMatrix4fv(mainProgram.uNormalMatrix, 1, GL_FALSE, 
            glm::value_ptr(glm::transpose(glm::inverse(earthMVMatrix))));
        glUniformMatrix4fv(mainProgram.uMVPMatrix, 1, GL_FALSE, 
            glm::value_ptr(ProjMatrix * earthMVMatrix));
        
        glUniform3fv(mainProgram.uKd, 1, glm::value_ptr(Kd));
        glUniform3fv(mainProgram.uKs, 1, glm::value_ptr(Ks));
        glUniform1f(mainProgram.uShininess, 5.);
        glUniform3fv(mainProgram.uLightDir_vs, 1, glm::value_ptr(LightDir));
        glUniform3fv(mainProgram.uLightIntensity, 1, glm::value_ptr(LightIntensity));

        glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
        
        // 32 Lunes
        // for (uint32_t i=0; i<nbmoon; ++i){
            
        //     glm::mat4 MVMatrix2 = glm::rotate(VMatrix, windowManager.getTime(), axesRotLunes[i]);
        //     MVMatrix2 = glm::translate(MVMatrix2, originLunes[i]); //translation
        //     MVMatrix2 = glm::scale(MVMatrix2, glm::vec3(0.2,0.2,0.2)); //scale
            
        //     glm::mat4 MVPMatrix2 = ProjMatrix*MVMatrix2;

        //     glUniformMatrix4fv(mainProgram.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(MVPMatrix2));
        //     glUniformMatrix4fv(mainProgram.uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVMatrix2));
        //     glUniformMatrix4fv(mainProgram.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix));
            
            
        //     glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
        // }
        glBindVertexArray(0);

        // Update the display
        windowManager.swapBuffers();
        
    }

    glDeleteBuffers(1,&vbo);
    glDeleteVertexArrays(1, &vao);

    return EXIT_SUCCESS;
}
