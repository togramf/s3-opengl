#include <GL/glew.h>
#include <iostream>
#include <vector>

#include "glimac/Sphere.hpp"
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "glimac/glm.hpp"
#include <glimac/SDLWindowManager.hpp>
#include "glimac/Image.hpp"
#include "glimac/TrackballCamera.hpp"

using namespace glimac;

struct EarthProgram {
    Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uEarthTexture;
    GLint uCloudTexture;

    EarthProgram(const FilePath& applicationPath):
        m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                              applicationPath.dirPath() + "shaders/multiTex3D.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
        uEarthTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture");
        uCloudTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture2");
    }
};

struct MoonProgram {
    Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uTexture;

    MoonProgram(const FilePath& applicationPath):
        m_Program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                              applicationPath.dirPath() + "shaders/tex3D.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
        uTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture");
    }
};

int main(int argc, char** argv) {
    // Initialize SDL and open a window
    SDLWindowManager windowManager(800, 600, "GLImac");

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    //chargement des textures (ATTENTION : le chemin est en absolu ! à reconfigurer sur chaque machine !)
    std::unique_ptr<Image> earth = loadImage ("/home/togram/Documents/IMAC/SI/S3-OpenGL_TP/GLImac-Template/assets/textures/EarthMap.jpg");
    if(!earth) {
        std::cerr << "oups ptr texture pabon" << std::endl;
        return EXIT_FAILURE;
    }
    std::unique_ptr<Image> moon = loadImage ("/home/togram/Documents/IMAC/SI/S3-OpenGL_TP/GLImac-Template/assets/textures/MoonMap.jpg");
    if(!moon) {
        std::cerr << "oups ptr texture pabon" << std::endl;
        return EXIT_FAILURE;
    }
    std::unique_ptr<Image> cloud = loadImage ("/home/togram/Documents/IMAC/SI/S3-OpenGL_TP/GLImac-Template/assets/textures/CloudMap.jpg");
    if(!cloud) {
        std::cerr << "oups ptr texture pabon" << std::endl;
        std::cerr << "oups ptr texture pabon" << std::endl;
        return EXIT_FAILURE;
    }

    //chargement des shaders
    FilePath applicationPath(argv[0]);
    EarthProgram earthProgram(applicationPath);
    MoonProgram moonProgram(applicationPath);

    glEnable(GL_DEPTH_TEST); 
    
    //création de la camera
    TrackballCamera* cam = new TrackballCamera();
    float pas = 5.0;

    //initialisation des matrices 
    glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f),(float)800/(float)600,0.1f,100.f);
    // glm::mat4 MVMatrix = glm::translate(glm::mat4(1), glm::vec3(0.,0.,-5.));
    
    glm::mat4 MVMatrix = cam->getViewMatrix();
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    
    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/
    
    //creation d'une sphère
    Sphere sphere(1,10,10);

    //Création d'un VBO
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData (GL_ARRAY_BUFFER, sphere.getVertexCount() * sizeof(ShapeVertex), sphere.getDataPointer(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Création de la VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_NORMALE = 1;
    const GLuint VERTEX_ATTR_COORDS = 2;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_COORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMALE);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex) , (const void*)(offsetof(ShapeVertex,position)));
    glVertexAttribPointer(VERTEX_ATTR_NORMALE, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void*)(offsetof(ShapeVertex,normal)));
    glVertexAttribPointer(VERTEX_ATTR_COORDS, 2, GL_FLOAT, GL_FALSE,sizeof(ShapeVertex), (const void*)(offsetof(ShapeVertex,texCoords)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Création de textures 
        // terre
    GLuint texture_earth;
    glGenTextures(1, &texture_earth);
    glBindTexture(GL_TEXTURE_2D, texture_earth);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, earth->getWidth(), earth->getHeight(), 0, GL_RGBA, GL_FLOAT, earth->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

        // lune
    GLuint texture_moon;
    glGenTextures(1, &texture_moon);
    glBindTexture(GL_TEXTURE_2D, texture_moon);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, moon->getWidth(), moon->getHeight(), 0, GL_RGBA, GL_FLOAT, moon->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);

        // nuages 
    GLuint texture_cloud;
    glGenTextures(1, &texture_cloud);
    glBindTexture(GL_TEXTURE_2D, texture_cloud);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, cloud->getWidth(), cloud->getHeight(), 0, GL_RGBA, GL_FLOAT, cloud->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);


    // Tirage des 32 axes de rotation des Lunes
    std::vector<glm::vec3> axesRotLunes;
    std::vector<glm::vec3> originLunes;
    float radiusRotLunes = 2.0;
    uint32_t nbmoon = 32;

    for (uint32_t i=0; i<nbmoon; ++i){
        
        glm::vec3 axe1 = glm::sphericalRand(radiusRotLunes);
        glm::vec3 axe2 = glm::sphericalRand(radiusRotLunes);
        glm::vec3 axe3 = glm::cross(axe1,axe2);
        float radiusLunes = glm::linearRand(1.5,3.0);

        while (glm::distance(glm::vec3(0.f,0.f,0.f),axe3)<1.0){
            axe1 = glm::sphericalRand(radiusRotLunes);
            axe2 = glm::sphericalRand(radiusRotLunes);
            axe3 = glm::cross(axe1,axe2);
        }
        axesRotLunes.push_back(glm::normalize(axe1));
        originLunes.push_back(glm::normalize(axe3) * radiusLunes);
    }

    // Application loop:
    bool done = false;
    while(!done) {
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true; // Leave the loop after this iteration
            }
        }
        if (windowManager.isKeyPressed(SDLK_z)||windowManager.isKeyPressed(SDLK_UP)){
            cam->rotateUp(-pas);
        } else if (windowManager.isKeyPressed(SDLK_s)||windowManager.isKeyPressed(SDLK_DOWN)){
            cam->rotateUp(pas);
        } else if (windowManager.isKeyPressed(SDLK_q)||windowManager.isKeyPressed(SDLK_LEFT)){
            cam->rotateLeft(-pas);
        } else if (windowManager.isKeyPressed(SDLK_d)||windowManager.isKeyPressed(SDLK_RIGHT)){
            cam->rotateLeft(pas);
        } else if (windowManager.isKeyPressed(SDLK_LSHIFT)||windowManager.isKeyPressed(SDLK_RSHIFT)){
            cam->moveFront(pas/10);
        } else if (windowManager.isKeyPressed(SDLK_LCTRL)||windowManager.isKeyPressed(SDLK_RCTRL)){
            cam->moveFront(-pas/10);
        } else if (windowManager.isKeyPressed(SDLK_i)){
            cam->initialise();
        }

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        MVMatrix = cam->getViewMatrix();

        glBindVertexArray(vao);
       
        // Planete
        earthProgram.m_Program.use();
        glUniform1i(earthProgram.uEarthTexture, 0);
        glUniform1i(earthProgram.uCloudTexture, 1);

        glm::mat4 earthMVMatrix = glm::rotate(MVMatrix, windowManager.getTime(), glm::vec3(0, 1, 0));
        
        glUniformMatrix4fv(earthProgram.uMVMatrix, 1, GL_FALSE, 
            glm::value_ptr(earthMVMatrix));
        glUniformMatrix4fv(earthProgram.uNormalMatrix, 1, GL_FALSE, 
            glm::value_ptr(glm::transpose(glm::inverse(earthMVMatrix))));
        glUniformMatrix4fv(earthProgram.uMVPMatrix, 1, GL_FALSE, 
            glm::value_ptr(ProjMatrix * earthMVMatrix));

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture_earth);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture_cloud);

        glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        
        // 32 Lunes
        moonProgram.m_Program.use();
        glUniform1i(moonProgram.uTexture, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture_moon);

        for (uint32_t i=0; i<nbmoon; ++i){
            
            glm::mat4 MVMatrix2 = glm::rotate(MVMatrix, windowManager.getTime(), axesRotLunes[i]);
            MVMatrix2 = glm::translate(MVMatrix2, originLunes[i]); //translation
            MVMatrix2 = glm::scale(MVMatrix2, glm::vec3(0.2,0.2,0.2)); //scale
            
            glm::mat4 MVPMatrix2 = ProjMatrix*MVMatrix2;

            glUniformMatrix4fv(moonProgram.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(MVPMatrix2));
            glUniformMatrix4fv(moonProgram.uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVMatrix2));
            glUniformMatrix4fv(moonProgram.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix));
            
            
            glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
        }
        glBindVertexArray(0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // Update the display
        windowManager.swapBuffers();
        
    }

    glDeleteBuffers(1,&vbo);
    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1, &texture_earth);
    glDeleteTextures(1, &texture_moon);
    glDeleteTextures(1, &texture_cloud);

    return EXIT_SUCCESS;
}
