#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include "glimac/Sphere.hpp"
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "glimac/glm.hpp"
#include <vector>

using namespace glimac;

int main(int argc, char** argv) {
    // Initialize SDL and open a window
    SDLWindowManager windowManager(800, 600, "GLImac");

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

     //chargement des shaders
    FilePath applicationPath(argv[0]);
    Program program = loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                                applicationPath.dirPath() + "shaders/normals.fs.glsl");
    
    program.use();

    //recup localisation variable uniforme 
    GLint locMVP = glGetUniformLocation(program.getGLId(),"uMVPMatrix");
    GLint locMV = glGetUniformLocation(program.getGLId(),"uMVMatrix");
    GLint locNormal = glGetUniformLocation(program.getGLId(),"uNormalMatrix");

    glEnable(GL_DEPTH_TEST); 

    glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f),(float)800/(float)600,0.1f,100.f);
    glm::mat4 MVMatrix = glm::translate(glm::mat4(1), glm::vec3(0.,0.,-5.));
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    // Exemples de combinaisons de transformations
    // glm::mat4 MVMatrix = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5)); // Translation
    // MVMatrix = glm::rotate(MVMatrix, windowManager.getTime(), glm::vec3(0, 1, 0)); // Translation * Rotation
    // MVMatrix = glm::translate(MVMatrix, glm::vec3(-2, 0, 0)); // Translation * Rotation * Translation
    // MVMatrix = glm::scale(MVMatrix, glm::vec3(0.2, 0.2, 0.2)); // Translation * Rotation * Translation * Scale


    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/
    
    //creation d'une sphère
    Sphere sphere(1,10,10);

    //Création d'un VBO
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData (GL_ARRAY_BUFFER, sphere.getVertexCount() * sizeof(ShapeVertex), sphere.getDataPointer(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Création de la VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_NORMALE = 1;
    const GLuint VERTEX_ATTR_COORDS = 2;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_COORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMALE);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex) , (const void*)(offsetof(ShapeVertex,position)));
    glVertexAttribPointer(VERTEX_ATTR_NORMALE, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const void*)(offsetof(ShapeVertex,normal)));
    glVertexAttribPointer(VERTEX_ATTR_COORDS, 2, GL_FLOAT, GL_FALSE,sizeof(ShapeVertex), (const void*)(offsetof(ShapeVertex,texCoords)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Tirage des 32 axes de rotation des Lunes
    std::vector<glm::vec3> axesRotLunes;
    std::vector<glm::vec3> originLunes;
    float radiusRotLunes = 2.0;

    for (int i=0; i<32; i++){
        
        glm::vec3 axe1 = glm::sphericalRand(radiusRotLunes);
        glm::vec3 axe2 = glm::sphericalRand(radiusRotLunes);
        glm::vec3 axe3 = glm::cross(axe1,axe2);
        float radiusLunes = glm::linearRand(1.5,3.0);

        while (glm::distance(glm::vec3(0.f,0.f,0.f),axe3)<1.0){
            axe1 = glm::sphericalRand(radiusRotLunes);
            axe2 = glm::sphericalRand(radiusRotLunes);
            axe3 = glm::cross(axe1,axe2);
        }
        axesRotLunes.push_back(glm::normalize(axe1));
        originLunes.push_back(glm::normalize(axe3) * radiusLunes);
    }

    // Application loop:
    bool done = false;
    while(!done) {
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true; // Leave the loop after this iteration
            }
        }

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(vao);
        // Planete
        glm::mat4 MVPMatrix = ProjMatrix*MVMatrix;
        glUniformMatrix4fv(locMVP, 1, GL_FALSE, glm::value_ptr(MVPMatrix));
        glUniformMatrix4fv(locMV, 1, GL_FALSE, glm::value_ptr(MVMatrix));
        glUniformMatrix4fv(locNormal, 1, GL_FALSE, glm::value_ptr(NormalMatrix));

        glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
        
        // 32 Lunes #fesses
        for (int i=0; i<32; i++){
            glm::mat4 MVMatrix2 = glm::rotate(MVMatrix, windowManager.getTime(), axesRotLunes[i]);
            MVMatrix2 = glm::translate(MVMatrix2, originLunes[i]); //translation
            MVMatrix2 = glm::scale(MVMatrix2, glm::vec3(0.2,0.2,0.2)); //scale
            
            glm::mat4 MVPMatrix2 = ProjMatrix*MVMatrix2;

            glUniformMatrix4fv(locMVP, 1, GL_FALSE, glm::value_ptr(MVPMatrix2));
            glUniformMatrix4fv(locMV, 1, GL_FALSE, glm::value_ptr(MVMatrix2));
            glUniformMatrix4fv(locNormal, 1, GL_FALSE, glm::value_ptr(NormalMatrix));

            glDrawArrays(GL_TRIANGLES, 0, sphere.getVertexCount());
        }
        


        glBindVertexArray(0);

        // Update the display
        windowManager.swapBuffers();

        
    }

    glDeleteBuffers(1,&vbo);
    glDeleteVertexArrays(1, &vao);

    return EXIT_SUCCESS;
}
