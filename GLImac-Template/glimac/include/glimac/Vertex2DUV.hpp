#pragma once

#include "common.hpp"

namespace glimac {

    class Vertex2DUV {
        
    public:
        glm::vec2 _position;
        glm::vec2 _texture;

        //constructeur
        Vertex2DUV(const double x,const double y,const double u,const double v) : _position(x,y), _texture(u,v) {}
        ~Vertex2DUV() = default;
    };
        
}