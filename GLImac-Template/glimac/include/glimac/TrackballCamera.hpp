#pragma once

#include "glm.hpp"

namespace glimac {

    class TrackballCamera {
        
    private:
        float m_fDistance;  //distance par rapport au centre de la scène
        float m_fAngleX;    //angle autour de l'axe X (bas ou haut)
        float m_fAngleY;    //angle autour de l'axe Y (rotation droite ou gauche)

    public:
        //constructeur et destructeur
        TrackballCamera() : m_fDistance(-5.f), m_fAngleX(0.f), m_fAngleY(0.f) {}
        TrackballCamera(const float d,const float x,const float y) : m_fDistance(d), m_fAngleX(x), m_fAngleY(y) {}
        ~TrackballCamera() = default;

        //méthodes de déplacement 
        void moveFront(float delta){
            m_fDistance += delta;
        };

        void rotateLeft(float degrees){
            m_fAngleY += degrees;
        };

        void rotateUp(float degrees){
            m_fAngleX += degrees;
        };

        void initialise(){
            m_fDistance = -5.f;
            m_fAngleX = 0.f;
            m_fAngleY = 0.f;
        }

        /**
         * @brief Get the View Matrix object
         * 
         * @return glm::mat4 
         */
        glm::mat4 getViewMatrix() const {
            glm::mat4 VMatrix = glm::rotate(glm::mat4(1.0), glm::radians(m_fAngleX), glm::vec3(1, 0, 0)) ;
            VMatrix = glm::rotate(glm::mat4(1.0), glm::radians(m_fAngleY), glm::vec3(0, 1, 0)) * VMatrix;
            VMatrix = glm::translate(glm::mat4(1.0), glm::vec3(0.,0.,m_fDistance))* VMatrix;
            return VMatrix;
        }

    };
        
}