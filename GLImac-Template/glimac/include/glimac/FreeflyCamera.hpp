#pragma once

#include "glm.hpp"

namespace glimac {

    class FreeflyCamera {
        
    private:
        glm::vec3 m_Position;   //position de la cam dans le monde (eye)
        float m_fTheta;         //angle(rad) autour de l'axe X (bas ou haut)
        float m_fPhi;           //angle(rad) autour de l'axe Y (rotation droite ou gauche)
        glm::vec3 m_FrontVector;    // vecteur F
        glm::vec3 m_LeftVector;     // vecteur L
        glm::vec3 m_UpVector;       // vecteur U

        void computeDirectionVectors(){
            m_FrontVector = glm::vec3(cos(m_fTheta)*sin(m_fPhi), 
                            sin(m_fTheta), 
                            cos(m_fTheta)*cos(m_fPhi));
            
            m_LeftVector = glm::vec3(sin(m_fPhi+M_PI/2.f),
                            0,
                            cos(m_fTheta+M_PI/2.f));
            
            m_UpVector = glm::cross(m_FrontVector,m_LeftVector);
        }

    public:
        //constructeur et destructeur
        FreeflyCamera() : m_Position(0., 0., 0.), m_fTheta(0), m_fPhi(M_PI) {
            computeDirectionVectors();
        }
        ~FreeflyCamera() = default;

        //méthodes de déplacement 
        void moveFront(float t){
            m_Position += t * m_FrontVector;
            computeDirectionVectors();
        };

        void moveLeft(float t){
            m_Position += t * m_LeftVector;
            computeDirectionVectors();
        };

        void rotateLeft(float degrees){
            m_fPhi += glm::radians(degrees);
            computeDirectionVectors();
        };

        void rotateUp(float degrees){
            m_fTheta += glm::radians(degrees);
            computeDirectionVectors();
        };

        void initialise(){
            m_Position = glm::vec3(0.,0.,0.);
            m_fTheta = 0.;
            m_fPhi = 0.;
            computeDirectionVectors();
        }

        /**
         * @brief Get the View Matrix object
         * 
         * @return glm::mat4 
         */
        glm::mat4 getViewMatrix() const {
            glm::vec3 V = m_Position + m_FrontVector;
            return glm::lookAt(m_Position, V, m_UpVector);
        }
    };
        
}