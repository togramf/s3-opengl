#version 300 es

precision mediump float; 

in vec3 vVertexPosition;
in vec3 vVertexNormal;
in vec2 vVertexCoords;

uniform sampler2D uEarthTexture;
uniform sampler2D uCloudTexture;

out vec3 fFragColor; 


void main() {
    fFragColor = normalize(vVertexNormal);
    fFragColor = texture(uEarthTexture, vVertexCoords).xyz + texture(uCloudTexture, vVertexCoords).xyz;
};
