#version 300 es

precision mediump float; 

in vec3 vVertexPosition;
in vec3 vVertexNormal;
in vec2 vVertexCoords;

uniform sampler2D uTexture;

out vec3 fFragColor; 


void main() {
    fFragColor = normalize(vVertexNormal);
    fFragColor = texture(uTexture, vVertexCoords).xyz;
};
