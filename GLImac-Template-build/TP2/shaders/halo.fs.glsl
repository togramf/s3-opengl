#version 300 es

precision mediump float; 

in vec3 vFragColor;
in vec3 vFragPosition; 

vec3 centre_triangle = vec3(0.0,0.0,1.0);

out vec3 fFragColor; 

void main() {           
    float alpha = 2.0;
    float beta = 18.0 ; 
    float dist = distance(vFragPosition, centre_triangle);
    float attenuation = alpha * exp (- beta * dist * dist);
    
    fFragColor = vFragColor * attenuation;
};
